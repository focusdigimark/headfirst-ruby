# Headfirst ruby

## Prerequisites

1. Basic

**Variable**:

- Global
- Instance
- Class
- Constant

**Operators**

**Comments**

**If-Else Statement**

**Loops**

2. Method & Block

3. Class, Module

4. String & Regex

5. Array & Hash

6. DateTime

7. Iterators

8. File I/O

9. Exceptions

## Headfirst Advanced Ruby

1. OOP
2. HTTP Request
3. Database Access
4. Files : JSON, CSV
5. Email
6. Multithreading
7. Web Application
8. Socket
9. Web Service
10. Common gems

## Common gems

1. Bundle
2. Rake

## HeadFirst Simple REST API Applications

1.Ng7-Topics : https://guides.hanamirb.org/
2.ROR-Topics : https://rubyonrails.org/