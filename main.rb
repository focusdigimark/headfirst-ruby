#!/usr/bin/env ruby
require './secondary.rb'
puts "RUBY_VERSION:#{RUBY_VERSION}"
# global variable
$program_name = 'headfirst ruby'
# welcome to ruby
welcome

=begin
All Lessions For Prerequisite Parts:
1. Write a function format current time stamp to VN,EN date & time
Monday, %day, %month, %year
thu hai, ngay %day, thang %month, nam %year
=end
def ordinal_number_prefix(cardinal)
	# 0:th, 1:st,2:nd,3rd, 4th
	last_digit = cardinal.to_s.reverse![0].to_i
	return 'th' if last_digit == 0
	return 'st' if last_digit == 1
	return 'nd' if last_digit == 2
	return 'rd' if last_digit == 3
	return 'th'
end

def number_to_month(month, lang = en)
	months = %w[
		January
		February
		March
		April
		May
		June
		July
		August
		September
		October
		November
		December
	]
	months_vn = %w[
		Mot
		Hai
		Ba
		Tu
		Nam
		Sau
		Bay
		Tam
		Chin
		Muoi
		Muoi Mot
		Muoi Hai
	]
	lang_prefix = ''
	if lang == 'vn'
		months = months_vn
		lang_prefix = 'Thang '
	end
	return '' if month < 1 && month > 12
	return "#{lang_prefix}#{months[month-1]}"
end

def format_current_timestamp(lang = 'en')
	now = Time.now
	day = lang == 'en' ? "#{now.day}#{ordinal_number_prefix(now.day)}" : "Ngay #{now.day}"
	month = number_to_month(now.month, lang)
	year = now.year

	printf("%s, %s, %s", day, month, year)
end
# 1
puts format_current_timestamp()
puts format_current_timestamp('vn')
